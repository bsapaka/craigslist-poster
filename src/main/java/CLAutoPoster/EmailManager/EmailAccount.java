package CLAutoPoster.EmailManager;

public class EmailAccount {

    protected String address;

    protected String password;

    protected String imap;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setImap(String imap) {
        this.imap = imap;
    }

    public String getImap() {
        return imap;
    }
}
