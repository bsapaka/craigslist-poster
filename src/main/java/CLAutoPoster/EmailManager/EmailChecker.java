package CLAutoPoster.EmailManager;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.Properties;

public class EmailChecker {

    protected EmailAccount account;

    protected boolean connected = false;

    protected Store store;

    protected int maxCheckCount = 20;

    public ArrayList<Message> messages() {
        ArrayList<Message> messages = new ArrayList<Message>();
        if (connected) {
            try {
                Folder inbox = store.getFolder("INBOX");
                inbox.open(1);

                Message[] allMessages = inbox.getMessages();

                for(int i = allMessages.length - 1; i >= allMessages.length - 1 - maxCheckCount; i--) {
                    String from = InternetAddress.toString(allMessages[i].getFrom()).toLowerCase();
                    String subject =  allMessages[i].getSubject().toLowerCase();

                    if (from.contains("craigslist") && subject.contains("post/edit/delete")) {
                        messages.add(allMessages[i]);
                    }
                }

            } catch (NoSuchProviderException e) {
                e.printStackTrace();
                System.exit(1);
            } catch (MessagingException e) {
                e.printStackTrace();
                System.exit(2);
            }
        }
        return messages;
    }

    public EmailChecker connect() {
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");

        if(connected) return this;

        try {
            Session session = Session.getInstance(props, null);
            Store store = session.getStore();

            store.connect(
                    this.account.getImap(),
                    this.account.getAddress(),
                    this.account.getPassword()
            );

            this.store = store;

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        }

        this.connected = true;

        return this;
    }

    public EmailAccount getAccount() {
        return account;
    }

    public void setAccount(EmailAccount account) {
        this.account = account;
    }

    public int getMaxCheckCount() {
        return maxCheckCount;
    }

    public void setMaxCheckCount(int maxCheckCount) {
        this.maxCheckCount = maxCheckCount;
    }
}
