package CLAutoPoster.PostManager;

import org.openqa.selenium.By;

public class ResumePoster extends CLAutoPoster.PostManager.Poster {

    protected void navigateOptions() {
        driver.findElement(By.xpath("//*[@value='jw']")).click();
        driver.findElement(By.xpath("//*[@value='res']")).click();
        driver.findElement(By.xpath("//*[@value='1']")).click();
    }
}