package CLAutoPoster.PostManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

abstract class Poster {

    protected WebDriver driver;
    protected CLAutoPoster.PostManager.Post post;

    protected boolean phoneNumberIncluded = false;
    protected boolean nameIncluded = false;
    protected boolean locationIncluded = true;

    abstract void navigateOptions();

    public boolean navigateToForm() {
        driver.navigate().to("https://dallas.craigslist.org/");
        driver.findElement(By.id("post")).click();
        this.navigateOptions();
        return this.getDriver().getTitle().contains("create posting");
    }

    public void fillOutForm() {
        String emailAddress = post.getProfile().getEmail().getAddress();
        driver.findElement(By.id("FromEMail")).sendKeys(emailAddress);
        driver.findElement(By.id("ConfirmEMail")).sendKeys(emailAddress);

        if(this.phoneNumberIncluded()) {
            driver.findElement(By.id("contact_phone")).sendKeys(post.getProfile().getPhoneNumber());
        }

        if(this.nameIncluded()) {
            driver.findElement(By.id("contact_name")).sendKeys(post.getProfile().getName());
        }

        if(this.locationIncluded()) {
            driver.findElement(By.id("GeographicArea")).sendKeys(post.getLocation());
        }

        driver.findElement(By.id("PostingTitle")).sendKeys(post.getTitle());

        driver.findElement(By.id("postal_code")).sendKeys(post.getZip());

        driver.findElement(By.id("PostingBody")).sendKeys(post.getBody());

    }

    protected void fillOutSpecialFields() { }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public CLAutoPoster.PostManager.Post getPost() {
        return post;
    }

    public void setPost(CLAutoPoster.PostManager.Post post) {
        this.post = post;
    }

    public boolean phoneNumberIncluded() {
        return phoneNumberIncluded;
    }

    public void setPhoneNumberIncluded(boolean phoneNumberIncluded) {
        this.phoneNumberIncluded = phoneNumberIncluded;
    }

    public boolean nameIncluded() {
        return nameIncluded;
    }

    public void setNameIncluded(boolean nameIncluded) {
        this.nameIncluded = nameIncluded;
    }

    public boolean locationIncluded() {
        return locationIncluded;
    }

    public void setLocationIncluded(boolean locationIncluded) {
        this.locationIncluded = locationIncluded;
    }
}
