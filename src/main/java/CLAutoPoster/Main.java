package CLAutoPoster;

import CLAutoPoster.EmailManager.EmailAccount;
import CLAutoPoster.EmailManager.EmailChecker;
import CLAutoPoster.PostManager.Post;
import CLAutoPoster.PostManager.ResumePoster;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;


public class Main {

    public static void main(String[] args) {
        testMail1();
    }

    public static void testMail1() {
        EmailAccount myEmail = new EmailAccount();
        myEmail.setAddress("brietsparks@gmail.com");
        myEmail.setPassword("asdf#F$G%H");
        myEmail.setImap("imap.gmail.com");

        EmailChecker checker = new EmailChecker();
        checker.setAccount(myEmail);

        ArrayList<Message> messages = checker.connect().messages();

        for (Message message : messages) {
            String body = "";
            try {
                MimeMultipart parts = (MimeMultipart) message.getContent();
                for(int i = 0; i < parts.getCount(); i++) {
                    body = body + parts.getBodyPart(i).getContent().toString();
                }

                Document doc = Jsoup.parse(body);
                Object[] links = doc.select("a[href]").toArray();

                for(int i = 0; i < links.length; i++) {
                    System.out.println(Jsoup.parse(links[i].toString()).text());
                }


            } catch (MessagingException e) {

            } catch (IOException e) {

            }


        }


//        Message[] messages = checker.connect().messages();

//        int n = 1;

//        ArrayList<Message> clMessages = new ArrayList<Message>();
//        for(int i = messages.length - 1; i >= 0; i--) {
//            if (n==1) {
//                try {
//                    String addressFrom = InternetAddress.toString(messages[i].getFrom());
//                    if(addressFrom.toLowerCase().contains("craigslist")) {
//                        clMessages.add(messages[i]);
//                    }
//
//                } catch (MessagingException e) {
//
//                }
//            } else if (n==2) {
//                try {
//                    System.out.println(messages[i].getContent());
//                } catch (MessagingException e) {
//
//                } catch (IOException e) {
//
//                }
//            }
//        }

    }

    public static void testMail() {
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");

        try {
            Session session = Session.getInstance(props, null);
            Store store = session.getStore();
            store.connect("imap.gmail.com", "brietsparks@gmail.com", "asdf#F$G%H");
            Folder inbox = store.getFolder("INBOX");
            inbox.open(1);
            Message[] messages = inbox.getMessages();

            for(int i = 0; i < (messages.length) / 5; i++) {
                System.out.println(messages[i].getSubject());
            }

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            System.exit(11);
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        }

    }

    public static void testPost() {
        WebDriver driver = new FirefoxDriver();

        EmailAccount myEmail = new EmailAccount();
        myEmail.setAddress("bws46@cornell.edu");
        Profile me = new Profile();
        me.setEmail(myEmail);
        me.setName("Briet");
        me.setPhoneNumber("469-408-6438");

        Post post = new Post();
        post.setProfile(me);
        post.setLocation("Plano");
        post.setTitle("First post");
        post.setZip("75093");
        post.setBody("This is the post body here");

        ResumePoster resumePoster = new ResumePoster();
        resumePoster.setDriver(driver);
        resumePoster.setPost(post);
        resumePoster.setLocationIncluded(true);
        resumePoster.setPhoneNumberIncluded(true);
        resumePoster.setNameIncluded(true);

        if( resumePoster.navigateToForm() ) {
            resumePoster.fillOutForm();
        }
    }
}
