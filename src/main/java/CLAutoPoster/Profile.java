package CLAutoPoster;

import CLAutoPoster.EmailManager.EmailAccount;

public class Profile {
    protected EmailAccount email;
    protected String phoneNumber;
    protected String name;

    public EmailAccount getEmail() {
        return email;
    }

    public void setEmail(EmailAccount email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
